[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).
### To access pages of app, use url listed below. ###
## URL, Action, Purpose  ##
* /users , index, page to list all users

* /users/1 , show, page to show user with id 1

* /users/new , new, page to make a new user

* /users/1/edit , edit, page to edit user with id 1

* /microposts , index, page to list all microposts

* /microposts/1 , show, page to show micropost with id 1

* /microposts/new	, new, page to make a new micropost

* /microposts , create, create a new micropost

* /microposts/1/edit , edit, page to edit micropost with id 1

* /microposts/1 , update, update micropost with id 1

* /microposts/1 , destroy, delete micropost with id 1